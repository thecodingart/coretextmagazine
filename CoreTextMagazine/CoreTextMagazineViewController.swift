//
//  ViewController.swift
//  CoreTextMagazine
//
//  Created by Brandon Levasseur on 6/23/15.
//  Copyright © 2015 Brandon Levasseur. All rights reserved.
//

import UIKit

class CoreTextMagazineViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("zombies", ofType: "txt")
        do {
            let text = try NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding) as String
            let parser = MarkupParser()
            let attributedString = parser.attributedStringFromMarkup(text)
            let ctView = view as! CTView
            ctView.attributedString = attributedString
            ctView.images = parser.images
            ctView.buildFrames()
        } catch let error {
            print("There was an error loading the file: \(error)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

