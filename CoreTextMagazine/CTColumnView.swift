//
//  CTColumnView.swift
//  CoreTextMagazine
//
//  Created by Brandon Levasseur on 6/23/15.
//  Copyright © 2015 Brandon Levasseur. All rights reserved.
//

import UIKit
import CoreText

class CTColumnView: UIView {
    var frameRef: CTFrameRef!
    var images = [[AnyObject]]()
    
    override func drawRect(rect: CGRect) {
        guard frameRef != nil else{
            return
        }
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSetTextMatrix(context, CGAffineTransformIdentity)
        CGContextTranslateCTM(context, 0, bounds.size.height)
        CGContextScaleCTM(context, 1.0, -1.0)
        CTFrameDraw(frameRef, context)
        
        for imageData in images {
            let image = imageData.first! as! UIImage
            let imageBounds = CGRectFromString(imageData[1] as! String)
            CGContextDrawImage(context, imageBounds, image.CGImage)
        }
    }
}
