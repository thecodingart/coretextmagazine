//
//  CTView.swift
//  CoreTextMagazine
//
//  Created by Brandon Levasseur on 6/23/15.
//  Copyright © 2015 Brandon Levasseur. All rights reserved.
//

import UIKit
import CoreText


class CTView: UIScrollView, UIScrollViewDelegate {
    
    var attributedString: NSAttributedString?
    var frameXOffset: CGFloat = 0
    var frameYOffset: CGFloat = 0
    
    var frames = [CTFrameRef]()
    
    var images = [[String: AnyObject]]()
    
    func buildFrames() {
        frameXOffset = 20
        frameYOffset = 20
        pagingEnabled = true
        delegate = self
        frames.removeAll()
        
        let path = CGPathCreateMutable()
        let textFrame = CGRectInset(bounds, frameXOffset, frameYOffset)
        CGPathAddRect(path, nil, textFrame)
        
        let framesetter = CTFramesetterCreateWithAttributedString(attributedString! as CFAttributedStringRef)
        
        var textPosition = 0
        var columnIndex = 0
        
        while textPosition < attributedString?.length {
            let columnOffset = CGPoint(x: (CGFloat(columnIndex) + 1.0) * frameXOffset + CGFloat(columnIndex) * (textFrame.size.width/2), y: 20)
            let columnRect = CGRect(origin: CGPointZero, size: CGSize(width: CGRectGetWidth(textFrame)/2 - 10, height: CGRectGetHeight(textFrame) - 40))
            let path2 = CGPathCreateMutable()
            CGPathAddRect(path2, nil, columnRect)
            
            let frame = CTFramesetterCreateFrame(framesetter, CFRange(location: textPosition, length: 0), path2, nil)
            let frameRange = CTFrameGetVisibleStringRange(frame)
            
            let content = CTColumnView(frame: CGRect(origin: CGPointZero, size: CGSize(width: contentSize.width, height: contentSize.height)))
            content.backgroundColor = UIColor.clearColor()
            content.frame = CGRect(x: columnOffset.x, y: columnOffset.y, width: CGRectGetWidth(columnRect), height: CGRectGetHeight(columnRect))
            content.frameRef = frame
            attachImagesWithFrame(frame, inColumnView: content)
            frames.append(frame)
            addSubview(content)
    
            textPosition += frameRange.length
            columnIndex++
        }
        
        let totalPages = (columnIndex + 1) / 2
        contentSize = CGSizeMake(CGFloat(totalPages) * CGRectGetWidth(bounds), CGRectGetHeight(textFrame))
    }
    
    func attachImagesWithFrame(frame: CTFrameRef, inColumnView columnView: CTColumnView) {
        let lines = CTFrameGetLines(frame) as NSArray as! [CTLineRef]
        var origins = Array<CGPoint>(count: lines.count, repeatedValue: CGPointZero)
        CTFrameGetLineOrigins(frame, CFRange(location: 0, length: 0), &origins)
        
        var imageIndex = 0
        
        var nextImage = images[imageIndex]
        var imageLocation = nextImage["location"] as! Int
        
        let frameRange = CTFrameGetVisibleStringRange(frame)
        
        while imageLocation < frameRange.location {
            imageIndex++
            if imageIndex >= images.count {
                return //quit if no images for this column
            }
            
            nextImage = images[imageIndex]
            imageLocation = nextImage["location"] as! Int
        }
        
        var lineIndex = 0
        for line in lines {
            
            let runs = CTLineGetGlyphRuns(line) as NSArray as! [CTRunRef]
            for run in runs {
                let runRange = CTRunGetStringRange(run)
                if runRange.location <= imageLocation && runRange.location + runRange.length > imageLocation {
                    var runBounds = CGRectNull
                    var ascent: CGFloat = 0
                    var descent: CGFloat = 0
                    runBounds.size.width = CGFloat(CTRunGetTypographicBounds(run, CFRange(location: 0, length: 0), &ascent, &descent, nil))
                    runBounds.size.height = ascent + descent
                    var xOffset = CTLineGetOffsetForStringIndex(line, CTRunGetStringRange(run).location, nil)
                    let currentOrigin = origins[lineIndex]
                    runBounds.origin.x = currentOrigin.x + self.frame.origin.x + xOffset + frameXOffset
                    runBounds.origin.y = currentOrigin.y + self.frame.origin.y + frameYOffset
                    runBounds.origin.y -= descent
                    
                    let imageName = nextImage["fileName"] as! String
                    let image = UIImage(named: imageName)!
                    let pathRef = CTFrameGetPath(frame)
                    let columnRect = CGPathGetBoundingBox(pathRef)
                    let imageBounds = CGRectOffset(runBounds, columnRect.origin.x - frameXOffset - self.contentOffset.x, columnRect.origin.y - frameYOffset - self.frame.origin.y)
                    columnView.images.append([image, NSStringFromCGRect(imageBounds)])
                    imageIndex++
                    if imageIndex < images.count {
                        nextImage = images[imageIndex]
                        imageLocation = nextImage["location"] as! Int
                    }
                }
            }
            lineIndex++
        }
        
    }
    
}
