//
//  MarkupParser.swift
//  CoreTextMagazine
//
//  Created by Brandon Levasseur on 6/23/15.
//  Copyright © 2015 Brandon Levasseur. All rights reserved.
//

import UIKit
import CoreText

class MarkupParser {
    var font = "ArialMT"
    var color = UIColor.blackColor()
    var strokeColor = UIColor.whiteColor()
    var strokeWidth: Float = 0
    var images = [[String: AnyObject]]()
    
    func attributedStringFromMarkup(markup: String) -> NSAttributedString? {
        let attributedString = NSMutableAttributedString(string: "")
        
        do {
            let regex = try NSRegularExpression(pattern: "(.*?)(<[^>]+>|\\Z)", options: NSRegularExpressionOptions(rawValue: (NSRegularExpressionOptions.CaseInsensitive.rawValue | NSRegularExpressionOptions.DotMatchesLineSeparators.rawValue)))
            let checkingResults = regex.matchesInString(markup, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, markup.characters.count))
            
            
            for checkingResult in checkingResults {
                let startIndex = advance(markup.startIndex, checkingResult.range.location)
                let endIndex = advance(startIndex, checkingResult.range.length)
                let parts = markup.substringWithRange(Range(start: startIndex, end: endIndex)).componentsSeparatedByString("<")
                let fontRef = CTFontCreateWithName(font, 24.0, nil)
                let attributes: [String: AnyObject] = [kCTForegroundColorAttributeName as String: color.CGColor, kCTFontAttributeName as String: fontRef, kCTStrokeColorAttributeName as String: strokeColor.CGColor, kCTStrokeWidthAttributeName as String: strokeWidth]
                attributedString.appendAttributedString(NSAttributedString(string: parts.first!, attributes: attributes))
                
                if parts.count > 1 {
                    let tag = parts[1]
                    switch tag {
                    case let tag where tag.hasPrefix("font"):
                        
                        let scolorRegex = try NSRegularExpression(pattern: "(?<=strokeColor=\")\\w+", options: NSRegularExpressionOptions(rawValue: 0))
                        scolorRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                            let startIndex = advance(tag.startIndex, match!.range.location)
                            let endIndex = advance(startIndex, match!.range.length)
                            let subTag = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                            switch subTag {
                            case "none":
                                self.strokeWidth = 0.0
                            default:
                                self.strokeWidth = -3.0
                                let colorSel = Selector("\(subTag)Color")
                                
                                self.strokeColor = UIColor.swift_performSelector(colorSel, withObject: nil) as! UIColor
                                
                            }
                        }
                        
                        
                        let colorRegex = try NSRegularExpression(pattern: "(?<=color=\")\\w+", options: NSRegularExpressionOptions(rawValue: 0))
                        colorRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                            let startIndex = advance(tag.startIndex, match!.range.location)
                            let endIndex = advance(startIndex, match!.range.length)
                            let subTag = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                            let colorSel = Selector("\(subTag)Color")
                            self.color = UIColor.swift_performSelector(colorSel, withObject: nil) as! UIColor
                        }
                        
                        let faceRegex = try NSRegularExpression(pattern: "(?<=face=\")[^\"]+", options: NSRegularExpressionOptions(rawValue: 0))
                        faceRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                            let startIndex = advance(tag.startIndex, match!.range.location)
                            let endIndex = advance(startIndex, match!.range.length)
                            self.font = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                            
                        }
                        

                        break
                    case let tag where tag.hasPrefix("img"):
                        
                        do {
                            var width = 0
                            var height = 0
                            var fileName = ""
                            let widthRegex = try NSRegularExpression(pattern: "(?<=width=\")[^\"]+", options: NSRegularExpressionOptions(rawValue: 0))
                            
                            widthRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                                let startIndex = advance(tag.startIndex, match!.range.location)
                                let endIndex = advance(startIndex, match!.range.length)
                                let subTag = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                                width = Int(subTag)!
                            }
                            
                            let heightRegex = try NSRegularExpression(pattern: "(?<=height=\")[^\"]+", options: NSRegularExpressionOptions(rawValue: 0))
                            
                            heightRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                                let startIndex = advance(tag.startIndex, match!.range.location)
                                let endIndex = advance(startIndex, match!.range.length)
                                let subTag = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                                height = Int(subTag)!
                            }
                            
                            let srcRegex = try NSRegularExpression(pattern: "(?<=src=\")[^\"]+", options: NSRegularExpressionOptions(rawValue: 0))
                            
                            srcRegex.enumerateMatchesInString(tag, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: tag.characters.count)) { (match, flags, stop) -> Void in
                                let startIndex = advance(tag.startIndex, match!.range.location)
                                let endIndex = advance(startIndex, match!.range.length)
                                let subTag = tag.substringWithRange(Range(start: startIndex, end: endIndex))
                                fileName = subTag
                            }
                            
                            images.append(["width": width, "height": height, "fileName": fileName, "location": attributedString.length])
                            
                            var callbacks = CTRunDelegateCallbacks(version: kCTRunDelegateVersion1, dealloc: deallocCallback, getAscent: ascentCallback, getDescent: descentCallback, getWidth: widthCallback)
                            
                            let imageAttributes = ["width": width, "height": height]
                            let unsafeImageAttributes = UnsafeMutablePointer<[String: Int]>.alloc(1)
                            unsafeImageAttributes.initialize(imageAttributes)
                            print(unsafeImageAttributes.memory)
                            let delegate = CTRunDelegateCreate(&callbacks, unsafeImageAttributes)!
                            let attributeDictionaryDelegate = [kCTRunDelegateAttributeName as String: delegate]
                            attributedString.appendAttributedString(NSAttributedString(string: " ", attributes: attributeDictionaryDelegate))
                            
                        } catch let error {
                            print("There was an error creating the expression: \(error)")
                            return nil
                        }
                        
                    default:
                        break
                    }
                }
            }
            
            
        } catch let error {
            print("There was an error generating the regex \(error)")
            return nil
        }
        
        return attributedString
    }
    
    
}

func deallocCallback(pointer: UnsafeMutablePointer<Void>) {
    pointer.destroy()
    pointer.dealloc(1)
}

func ascentCallback(pointer: UnsafeMutablePointer<Void>) -> CGFloat {
    let referenceDictionary = UnsafePointer<[String: Int]>(pointer)
    let height = referenceDictionary.memory["height"] ?? 0
    return CGFloat(height)
}

func descentCallback(pointer: UnsafeMutablePointer<Void>) -> CGFloat {
    let referenceDictionary = UnsafePointer<[String: Int]>(pointer)
    let descent = referenceDictionary.memory["descent"] ?? 0
    return CGFloat(descent)
}

func widthCallback(pointer: UnsafeMutablePointer<Void>) -> CGFloat {
    let referenceDictionary = UnsafePointer<[String: Int]>(pointer)
    let width = referenceDictionary.memory["width"] ?? 0
    return CGFloat(width)
}

