//
//  NSObject+Selector.h
//  CoreTextMagazine
//
//  Created by Brandon Levasseur on 6/23/15.
//  Copyright © 2015 Brandon Levasseur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Selector)

- (id)swift_performSelector:(SEL)selector withObject:(id)object;
- (void)swift_performSelector:(SEL)selector withObject:(id)object afterDelay:(NSTimeInterval)delay;

@end
